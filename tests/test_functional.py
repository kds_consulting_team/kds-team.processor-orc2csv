import os
import unittest
import logging

from datadirtest import DataDirTester


class TestComponent(unittest.TestCase):

    def test_functional(self):
        logging.info('Running functional tests')
        functional_tests = DataDirTester()
        functional_tests.run()


if __name__ == "__main__":
    unittest.main()
