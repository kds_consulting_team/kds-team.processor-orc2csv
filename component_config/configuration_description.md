Taking all ORC files from `/data/in/files` and convert them to CSV format. All the converted CSV tables will be stored into `/data/out/files`

### Optional Parameters
1. columns
    - `Array` JSON input
    - allows users to have the option to select the columns they want to output
    - all columns specified in this parameter will need to exist in the ORC file

### Sample Configuration

Default Parameters
```
{
    "definition": {
        "component": "kds-team.processor-orc2csv"
    }
}
```

With optional parameters
```
{
    "definition": {
        "component": "kds-team.processor-orc2csv"
    },
    "parameters": {
        "columns": [
            "column_1",
            "column_2"
        ]
    }
}
```