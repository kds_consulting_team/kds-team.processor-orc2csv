from keboola.component import ComponentBase, UserException
import csv
import logging

import pyorc
from pyorc.enums import StructRepr

# configuration variables
KEY_DEBUG = 'debug'

MANDATORY_PARS = []
MANDATORY_IMAGE_PARS = []

APP_VERSION = '0.2.2'


class Component(ComponentBase):

    def __init__(self):
        # for easier local project setup
        ComponentBase.__init__(self)
        self.cfg_params = self.configuration.parameters

    def run(self):
        # Get proper list of tables
        in_files_defintions = self.get_input_files_definitions()
        in_files = [file_def.full_path for file_def in in_files_defintions]
        logging.info(f'Number of files to convert: [{len(in_files)}]')

        params = self.cfg_params
        columns = params.get('columns')

        # Validating processor input
        if not isinstance(columns, list) and columns is not None:
            raise UserException('[columns] specification has to be a list.')

        # Parsing ORC to CSV
        for file in in_files:
            logging.info(f'Converting [{file}]')
            output_destination = file.replace('in/files', 'out/files')
            output_destination = output_destination.replace('.orc', '.csv')

            # Reader/Writer module
            with open(file, 'rb') as reader, open(output_destination, 'w', newline='') as writer:

                # Input
                try:
                    orc_reader = pyorc.Reader(
                        reader, struct_repr=StructRepr.DICT)
                except Exception as err:
                    logging.error(f'[{file}] parsing error - {err}')
                file_columns = [header for header in list(
                    orc_reader.schema.fields)]

                # Validate if the iput columns exist
                if columns:
                    self.validate_input_columns(
                        file_name=file,
                        input_columns=columns,
                        file_columns=file_columns
                    )
                    file_columns = columns

                # Output
                dict_writer = csv.DictWriter(writer, fieldnames=file_columns)
                dict_writer.writeheader()

                # Parsing/Outputting
                for row in orc_reader:
                    data = {}

                    if columns:
                        for col in columns:
                            data[col] = row.get(col)
                    else:
                        data = row

                    dict_writer.writerow(data)

        logging.info("Processor orc2csv finished")

    def validate_input_columns(self, file_name, input_columns: list, file_columns: list):
        '''
        Ensure all the columns are available in the file
        '''

        for col in input_columns:
            if col not in file_columns:
                raise UserException(f'[{col}] does not exist in [{file_name}]')


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
